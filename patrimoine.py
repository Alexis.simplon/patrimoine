import pandas as pd
import json
import sqlite3 as sql3
from sqlite3 import Error as e
import numpy as np
import re
import time
import folium
from folium import FeatureGroup,LayerControl,plugins
from folium.plugins import BeautifyIcon,MarkerCluster,FeatureGroupSubGroup,LocateControl, Fullscreen
import os
from os import path

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except e as er:
        print(er)
def insert_tables(conn,nom_batiment,lat,lon,texte,theme,liste_categorie):
    '''
    Insert dans la table pays le nom du pays si il n'es pas déja dans la database
    '''
    
    sql_bat = f''' INSERT INTO batiment (titre,lat,lon,texte) VALUES(?,?,?,?)'''
    sql_theme = f''' INSERT INTO thematique (nom,couleur,icone) VALUES(?,?,?)'''
    sql_liaison=f''' INSERT INTO thematique_batiment (id_batiment,id_theme) VALUES(?,?)'''
    cur = conn.cursor()
    liste_batiment=[nom_batiment,float(lat),float(lon),texte]
    select_bat =[('titre',nom_batiment),('lat',lat),('lon',lon)]
    
    
        #conn, nom_table,condition_rechercher='*', nom_colonne='',valeur_condition=''
    if select_table(conn,'batiment','*',select_bat)==[]:
        #print(select_table(conn,'batiment','*',select_bat))
        try:
            cur.execute(sql_bat,liste_batiment)
            conn.commit()
        except e as er:
            print(er)
    elif select_table(conn,'batiment','*',select_bat)!=None:
        print('le patrimoine es inserer')

    else:
        print("l'insert du batiment a echoue")
    if select_table(conn,'thematique','*','nom',theme)==[]:
        for categorie in liste_categorie:
            #print(f"categorie = {categorie} en dessous theme :")
            #print(theme)
            if theme == categorie[1]:
                try:
                    info_theme=[theme,categorie[2],categorie[3]]
                    cur.execute(sql_theme,info_theme)
                    conn.commit()
                except e as er:
                    print(er)
    else:
        print('la categorie existe deja')
    try:
        id_theme=select_table(conn,'thematique','id','nom',theme)
        id_bat=select_table(conn,"batiment","id",select_bat)
        print(f"id batiment :{id_bat}")
        print(f"id theme :{id_theme}")
    except e as er:
        print(er)
    if id_bat!= [] and id_theme!=[]:
        try:
            cur.execute(sql_liaison,[id_bat[0],id_theme[0]])
            conn.commit()
        except e as er:
            print(er)

 
def select_table(conn, nom_table,condition_rechercher='*', nom_colonne='',valeur_condition=''):
    """ select 
    :param conn: Connection object
    :param nom_table: name of table for select
    :param nom_colone: soit le nom d'une colonne soit une liste comprenant un tuple ou une liste de nom colonne et de valeur
    :return:
    """
    if type(condition_rechercher) is list:
        condition=' '
        for cond in condition_rechercher:
            condition+=f'{cond}, '
        condition_rechercher=condition[:-1]
    
    if type(nom_colonne) is list:
        where_sql = 'WHERE '
        for arg in nom_colonne:
            print("c bien une liste")
            val=arg[1]
            if type(arg[1]) is not float and type(arg[1]) is not int:
                val=f'"{arg[1]}"'
            where_sql+=f'{arg[0]} = {val} AND '
        nom_colonne=where_sql[0:-4]   
    elif valeur_condition=='':
        pass
    elif type(valeur_condition) is not int or type(valeur_condition) is not float:
        nom_colonne= f" WHERE {nom_colonne} = '{valeur_condition}'"
    else :
        f" WHERE {nom_colonne} = {valeur_condition}"


    try:
        c = conn.cursor()
        if nom_colonne !='':
            select =f" select {condition_rechercher} from {nom_table} {nom_colonne};"
        else:
            select =f" select {condition_rechercher} from {nom_table} ;"
        c.execute(select)
        print(select)
        if condition_rechercher =='id':
            data=c.fetchone()
        else:
            data=c.fetchall()
        
    except e as er:
        print(er)
        data=[]

    
    return data

def recuperer_csv(url):
    
    df2= pd.read_csv(url)# transforme un fichier csv en dataframe
    df2=df2[['Titre','Thématiques','Longitude','Latitude','Texte FR']]#permet de recuper seulement les colonnes choisi
    df2.rename(columns={'Texte FR':'Texte'}, inplace=True)
    #df.replace({'Longitude': 0,'Latitude':0}, None )
    #remplace la valeur de longitude quand elle est egale a zero par nan
    df2['Longitude'][df2['Longitude']==0]=np.nan
    df2['Latitude'][df2['Latitude']==0]=np.nan#meme chose avec latitude
    regex= "<[^\>]*>"#regex permetant de trouver les balises <?????>
    df2['Texte']= [re.sub(regex,'',elem) for elem in df2['Texte']]
    df2.dropna(inplace=True)#supprime les ligne comprenant les valeurs Nan
    return df2

def creation_group_list_cat(donnee,carte):
    '''
    dataframe es la dataframe comprenant la liste de thematique
    carte est la carte ou j'ajoute 
    '''
    liste_couleur=['red','#53210D','lightgreen','black','purple','blue'] # crée liste de couleur a ajouter au marker 
    liste_icone =['history','fort-awesome','tree','flask','paint-brush','umbrella']
    liste_categorie=[]
    liste_donne=cree_liste_categorie(donnee)
    print(liste_donne)
    #cree une liste comprenant les differentes categorie
    i=0
    for theme in liste_donne :
        #print(theme)
        #separe les differentes categorie separer par une virgule
        them = theme.strip()
        if them not in liste_categorie: #si le theme n'es pas dans la liste l'ajoute
            mcg = MarkerCluster(control=False, name = theme)    
            #car2=folium.plugins.FeatureGroupSubGroup(legende,theme)
            car=FeatureGroupSubGroup(mcg,theme)
            #m.add_child(mcg)  
            carte.add_child(mcg)
            #creer une liste de tuples comprenant la variable de sous groupe, le nom de la categorie et sa couleur
            carte.add_child(car)#ajoute la sous categorie a la carte
            #m.add_child(car2)
            liste_categorie.append((car,them))        
            i+=1

    return liste_categorie
def ajout_groupe_map(carte,liste_categorie: list):
    #cree une liste comprenant les differentes categorie
    for theme in liste_categorie :
        mcg = MarkerCluster(control=False, name = theme)    
        #car2=folium.plugins.FeatureGroupSubGroup(legende,theme)
        car=FeatureGroupSubGroup(mcg,theme)
        #m.add_child(mcg)  
        carte.add_child(mcg)
        #creer une liste de tuples comprenant la variable de sous groupe, le nom de la categorie et sa couleur
        carte.add_child(car)#ajoute la sous categorie a la carte
        #m.add_child(car2)
        

def cree_liste_categorie(list_theme):
    '''
    list_theme :sataframe ou liste comprenant differentes categorie 
    liste_categorie: liste_theme sans doublons 
    '''
    liste_categorie=[]
    for theme in list_theme :
        theme=theme.split(',')#separe les differentes categorie separer par une virgule
        for them in theme:
            if them.strip() not in liste_categorie: #si le theme n'es pas dans la liste l'ajoute
                liste_categorie.append(them.strip())
    #print(liste_categorie)
    return liste_categorie
def connexion(nombd):
    try:
        print(nombd)
        conn = sql3.connect(f'{nombd}')
        print(sql3.version)       
    except e as er:
        print(er)
    print(f'connexion : {conn}')
    return conn
def connex_creation_bd(nom_bd):
  
    conn=connexion(nom_bd)
    cur=conn.cursor()
    sql_batiment = """CREATE TABLE  if not exists batiment (   
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                titre TEXT,
                texte TEXT,
                lat float,
                lon float         
                );"""
    
    sql_thematique = """CREATE TABLE if not exists thematique 
                                (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                nom TEXT,
                                couleur TEXT,
                                icone TEXT
                                );"""
    sql_liaison="""CREATE TABLE if not exists thematique_batiment 
                    (
                    id_batiment int REFERENCES batiment(id) ON DELETE CASCADE,
                    id_theme int REFERENCES thematique(id) ON DELETE CASCADE,
                    primary key(id_batiment,id_theme)
                    );"""
    sql_select = 'select * from batiment'

    if conn is not None:
        try:
            create_table(conn, sql_batiment)
            create_table(conn, sql_thematique)
            create_table(conn, sql_liaison)
        except e as er:
            print(er)
    else:
        print("Error! cannot create the database connection.")
def cree_bd(nom_bd):
    path = os.getcwd()
    url ='http://entrepot.metropolegrenoble.fr/opendata/38185-GRE/Patrimoine/csv/PATRIMOINE_VDG.csv'

    conn= connexion(f'{path}/{nom_bd}.db')
    connex_creation_bd(f'{path}/{nom_bd}.db')
    import time
    carte=folium.Map(location=[45.1667,5.7167],zoom_start=10,tiles='Stamen Toner')
    df2 =recuperer_csv(url)
    liste_categorie=creation_group_list_cat(df2,carte)
    liste_thematique= cree_liste_categorie(df2['Thématiques'])
    
    df2=recuperer_csv(url)
    
    for i,row in df2.iterrows():
        them=row['Thématiques'].split(',')
        #print(row)
        for theme in them :
            theme=theme.strip()
            insert_tables(conn,row['Titre'],row['Latitude'],row['Longitude'],row['Texte'],theme,liste_categorie)
       
if __name__ == "__main__":
    #path = path.realpath('patrimoine.py')
    url ='http://entrepot.metropolegrenoble.fr/opendata/38185-GRE/Patrimoine/csv/PATRIMOINE_VDG.csv'

    df2 =recuperer_csv(url)
    liste = cree_liste_categorie(df2['Thématiques'])
    print(liste)
    cree_bd('mon_patrimoine')
    #path = path.
    